class CommissionEmployee extends Employee{
    constructor(firstName,lastName,socialSecurityNumber,grossSales,commissionRate){
        super(firstName,lastName,socialSecurityNumber)
        this.grossSales = grossSales;
        this.socialSecurityNumber = socialSecurityNumber;
    }
}
export { CommissionEmployee }
import { Employee } from "./Employee.js";