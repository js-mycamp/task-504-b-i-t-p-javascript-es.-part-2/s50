
import { Employee } from "./Employee.js";

class SalariedEmployee extends Employee {
    constructor(firstName,lastName,socialSecurityNumber,weekylySalary){
        super(firstName,lastName,socialSecurityNumber)
        this.weekylySalary = weekylySalary;
    }
}
export { SalariedEmployee }