import { Employee } from "./Employee.js";

class HourlyEmployee extends Employee {
    constructor(firstName,lastName,socialSecurityNumber,wage,hours){
        super(firstName,lastName,socialSecurityNumber)
        this.wage = wage;
        this.hours = hours;
    }
}
export { HourlyEmployee };