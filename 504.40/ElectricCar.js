import { Car } from "./Car.js";

 class ElectricCar extends Car {
    constructor(make, model, batteryLevel) {
        super(make, model)
        this.batteryLevel = batteryLevel;
    }
    change() {
        console.log("Change vừa được gọi", this.batteryLevel);
    }
}
export {ElectricCar}