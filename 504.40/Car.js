
 class Car {
    constructor(make, model) {
        this.make = make;
        this.model = model;
    }
    drive() {
        console.log("Drive vừa được gọi", this.model)
    }
}
export {Car}