import { Car } from "./Car.js";

  class PetrolCar extends Car {
    constructor(make, model, fuelLevel) {
        super(make, model,fuelLevel)
        this.fuelLevel = fuelLevel;
    }
    filUp(){
        console.log("FilUp vừa được gọi " , this.fuelLevel)
    }
}
export{PetrolCar}